module Main where

import           MerkleSpec (merkleSpec)
import           Test.Hspec

main :: IO ()
main = hspec $ do
  merkleSpec
