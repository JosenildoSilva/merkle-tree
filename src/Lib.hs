{-# LANGUAGE ScopedTypeVariables #-}

module Lib where

import           Crypto.Hash        (Digest, HashAlgorithm, SHA256 (..),
                                     digestFromByteString, hash)
import qualified Data.ByteArray     as ByteArray (concat, convert)
import           Data.ByteString    (ByteString)
import qualified Data.ByteString    as ByteString (reverse)
import qualified Data.Char          as Char (isHexDigit)
import           Data.Either        (Either (..))
import           Data.Function      ((&))
import qualified Data.HexString     as Hex (fromBytes, hexString, toBytes, toText)
import           Data.List          (find)
import           Data.Maybe         (fromJust)
import           Data.Text          (Text)
import qualified Data.Text          as Text (all, length, pack)
import qualified Data.Text.Encoding as Text (encodeUtf8)
import           Prelude            hiding (elem)

import           Types       (MerkleTree (..), Tx)
import           List         (bTw)
import           Tuple        (elem)

-- |< Combines two digests >|
cmb :: forall a. HashAlgorithm a => Digest a -> Digest a -> Digest a
cmb h g = merge h g
  & ByteString.reverse
  & dblHash
  & ByteArray.convert
  & ByteString.reverse
  & digestFromByteString
  & fromJust
  where
    merge x y = ByteArray.concat [y, x] :: ByteString
    dblHash a = hash (hash a :: Digest a) :: Digest a

-- |< To Hex representation SHA256 digest turns it into a Tx. >|
transTX :: Text -> Maybe Tx
transTX t
  | chkSHA256 t = parse t
  | otherwise   = Nothing
  where
    parse = digestFromByteString . Hex.toBytes . Hex.hexString . Text.encodeUtf8
   
-- |< Check if one Hex representation of a SHA256 digest it's an actual 'Digest SHA256'. >|
chkSHA256 :: Text -> Bool
chkSHA256 t = hasLength && isHex  
  where
    isHex     = Text.all Char.isHexDigit t
    hasLength = Text.length t == 64

-- |< Combines two Hex digests of SHA256 and returns that.  >|
cmbSHA256 :: Text -> Text -> Maybe Text
cmbSHA256 h g
  | not (chkSHA256 h && chkSHA256 g) = Nothing
  | otherwise = toHexText <$> maybeDigest
  where
    toHexText   = Hex.toText . Hex.fromBytes . ByteArray.convert
    maybeDigest = cmb <$> transTX h <*> transTX g

-- |< Calculates the Merkle Tree Root from one list of 'Tx'. >|
mklRoot :: [ Tx ] -> Digest SHA256
mklRoot txs
  | length reduced == 1 = head reduced
  | otherwise           = mklRoot reduced
  where
    reduced = map (uncurry cmb) $ bTw txs

-- |< Build a 'MerkleTree' from the given list of 'Tx'. >|
mklTree :: [ Tx ] -> MerkleTree (Digest SHA256)
mklTree = bTree . incLeafs

-- |< Include leafs on 'MerkleTree' >|
incLeafs :: [ Tx ] -> [ MerkleTree (Digest SHA256) ]
incLeafs txs = (\x -> Node x Leaf Leaf) <$> txs

-- |< 'MerkleTree' builder helper function >|
bTree :: [ MerkleTree (Digest SHA256) ] -> MerkleTree (Digest SHA256)
bTree [] = Leaf
bTree trees
  | length nodes == 1 = head nodes
  | otherwise         = bTree nodes
  where
    nodes = map (uncurry asNode) $ bTw trees
    
    asNode :: MerkleTree (Digest SHA256) -> MerkleTree (Digest SHA256) -> MerkleTree (Digest SHA256)
    asNode Leaf Leaf                     = Leaf
    asNode l@(Node x _ _) r@(Node y _ _) = Node (cmb x y) l r
    asNode _ _                           = Leaf

-- |< 'MerkleTree' proof function >|
mklPrf :: [ Tx ] -> Tx -> [ Either (Digest SHA256) (Digest SHA256) ]
mklPrf [] _   = []
mklPrf [_] _  = []
mklPrf txs tx =
  let
    sblOf :: Digest SHA256 -> [ (Digest SHA256, Digest SHA256) ] -> Either (Digest SHA256) (Digest SHA256)
    sblOf x = toEither . fromJust . find (elem x)
      where
        toEither (a, b)
          | a == x    = Right b
          | otherwise = Left a

    merge :: Either (Digest SHA256) (Digest SHA256) -> Digest SHA256 -> Digest SHA256
    merge (Left h) g  = cmb h g
    merge (Right h) g = cmb g h

    bTTxs = bTw txs
    sbl = sblOf tx bTTxs
  in
    sbl : mklPrf (uncurry cmb <$> bTTxs) (merge sbl tx)

-- |< Check if one Digest is valid >|
check :: Tx -> Digest SHA256 -> [ Either (Digest SHA256) (Digest SHA256) ] -> Bool
check tx mkr proof = (== mkr) $ foldl diag tx proof
  where
    diag :: Digest SHA256 -> Either (Digest SHA256) (Digest SHA256) -> Digest SHA256
    diag h (Left x)  = cmb x h
    diag h (Right x) = cmb h x
