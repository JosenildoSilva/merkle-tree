module List where


-- |< Join two adjancent elements in a list starting from the left. If the list is odd length the last element is joined with itself. >|
bTw :: [a] -> [(a, a)]
bTw []           = []
bTw [x]          = [(x, x)]
bTw (x : y : zs) = (x, y) : bTw zs
