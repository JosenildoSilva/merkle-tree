{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Crypto.Hash           (SHA256 (..), hashWith)
import           Lib                   (chkSHA256, transTX, mklTree)
import           Data.ByteString       (ByteString,uncons)
import qualified Data.ByteString.Char8 as BTS
import qualified Data.ByteString       as BTS
import           Data.Maybe            (isJust, fromJust, fromMaybe)
import qualified Data.Text             as T
import qualified Data.Text.Encoding    as T
import qualified Data.Text.IO          as T
import           Data.Text             (Text)
import           Types                 (Tx)

-- |< Process transactions file with guards to validade input with empty files >|
process :: ByteString -> IO ()
process inp
  | null (uncons inp) = print "Empty transactions file. Attempt with other file!"
  | otherwise         = let transactions = map T.decodeUtf8 (BTS.words inp)
                            toHash = fromJust . transTX
                            txs = map toHash (filter chkSHA256 transactions)
                            res = mklTree txs in
                                do
                                  print "###############"
                                  print "Merkle Tree ==>"
                                  print "###############"
                                  print res

main :: IO ()
main = do
  inp <- BTS.getContents
  process inp

