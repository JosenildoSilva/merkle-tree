<<<<<<< README.md
# merkle-tree

That sample merkle-tree contains all main process applied on proof and validations about insert transaction on one transactions block.

# requisites

- haskell-stack and haskell 8.10.3 (lts-17.2)

# unit tests

To execute tests you need on root directory execute:

- stack test

# main execution

To execute with data transactions sample file(262144 replicated transactions), for example, on root dir:

- cat input.txt | stack run --
- cat data.txt | stack run --

# comments

- If need execute on latest versions of ghc compiler need get nightly builds about cryptonite because the stable lib is not adjusted to latest stable haskell versions.

